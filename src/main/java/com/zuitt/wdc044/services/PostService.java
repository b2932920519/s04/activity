package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;

public interface PostService {

    //Create a post method
    void createPost(String stringToken, Post post);

    //Getting all posts
    Iterable<Post> getPosts();


}
