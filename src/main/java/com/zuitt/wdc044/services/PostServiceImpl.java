package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // method for creating post object
    @Override
    public void createPost(String stringToken, Post post) {
        //findByUsername to retrieve the user.
        //Criteria for finding the user is from the jwtToken method "getUsernameFromToken"
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        //The title and content will come from the reqBody which is passed through the "post" identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        //author retrieved from the token.
        newPost.setUser(author);

        //actual saving of the post object into our database.
        postRepository.save(newPost);
    }

    //method for getting all posts
    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }




}
