package com.zuitt.wdc044.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.zuitt.wdc044.models.User;


@Repository
public interface UserRepository extends CrudRepository<User, Object> {

    //A custom method for finding a user by their username.
    User findByUsername(String username);



}
